#### Репозиторий создан для размещения заданий по вычислительной практике
#### Славашевич Олег, 2 курс, 8 группа

#### Индивидуальные задания:
1. [Задание по алгоритмизации 1](https://bitbucket.org/slavaolegovich/2020-mmf-2-course/src/master/TaskAlgoritm/) 
2. [Задание по алгоритмизации 2](https://bitbucket.org/slavaolegovich/2020-mmf-2-course/src/master/TaskAlgoritm2/)
3. [Задание по алгоритмизации 3](https://bitbucket.org/slavaolegovich/2020-mmf-2-course/src/master/TaskAlgoritm3/)

#### Групповое задание 
Код группового задания находится [здесь](https://bitbucket.org/slavaolegovich/2020-mmf-2-course/src/master/optimized/).
Результат работы группового задания можно увидеть по [ссылке](https://vich-2020-mmf-2-course.herokuapp.com/)
