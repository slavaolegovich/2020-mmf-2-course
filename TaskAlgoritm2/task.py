def rotateInPlace(mat): 
    n = len(mat[0])  

    for i in range(0, int(n / 2)): 
          
        for j in range(i, n-i-1): 
              
            temp = mat[i][j] 
  
            mat[i][j] = mat[n-j-1][i] 
  
            mat[n-j-1][i] = mat[n-i-1][n-j-1] 
  
            mat[n-i-1][n-j-1]  = mat[j][n-i-1] 
  
            mat[j][n-i-1]  = temp 
    
    return mat
  
  
def displayMatrix( mat ): 
    n = len(mat[0]) 

    for i in range(0, n): 
          
        for j in range(0, n): 
              
            print (mat[i][j], end = ' ') 
        print ("") 

if __name__ == "__main__":
    mat = [
                    [1, 2, 3, 4], 
                    [5, 6, 7, 8], 
                    [9, 10, 11, 12], 
                    [13, 14, 15, 16]    ]

    displayMatrix(rotateInPlace(mat))