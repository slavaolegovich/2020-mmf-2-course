def editDist(s: str, t: str)->int:
    if len(s) == 0:
        return len(t)
    if len(t) == 0:
        return len(s)

    cost = 1 if s[-1] != t[-1] else 0

    d1 = editDist(s[:-1], t) + 1
    d2 = editDist(s, t[:-1]) + 1
    d3 = editDist(s[:-1], t[:-1]) + cost

    return min(d1, d2, d3)


if __name__ == "__main__":
    editDist('ab', 'ba')