from task import editDist

def Test(a, b):
    print("True") if a == b else print("False")

Test(editDist('', ''), 0)
Test(editDist('test', 'task'), 2)
Test(editDist('a', ''), 1)
Test(editDist('java', 'javascript'), 6)
Test(editDist('abc', 'abc'), 0)
Test(editDist('abc', 'acb'), 2)
Test(editDist('test', 'tests'), 1)