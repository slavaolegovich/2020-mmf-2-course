from flask import Flask
from flask import render_template
from flask import Flask, redirect, url_for, render_template, request
import templates

app = Flask(__name__)

@app.route('/', methods=["POST", "GET"])
def main():
    if request.method == "POST":
        st = request.form.get("str")
        return redirect(url_for("unstretch", s = st))
    else:
        return render_template("main.html")

@app.route('/<s>')
def unstretch(s):
    out = ''
    for i in range(len(s) - 1):
        if s[i] != s[i+1]: 
            out += s[i]
    if s[-1] != out[-1]:
         out += s[-1]
    return render_template("result.html", out = out)

if __name__ == "__main__":
    app.run(debug=True)