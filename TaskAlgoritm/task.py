def unstretch(s: str) -> str: 
    out = ''
    for i in range(len(s) - 1):
        if s[i] != s[i+1]: 
            out += s[i]
    if s[-1] != out[-1]:
         out += s[-1]
    return out