from datetime import datetime, timedelta
import timeit
import time 
from flask import Flask
from flask import render_template
from flask import Flask, redirect, url_for, render_template, request
import templates

app = Flask(__name__)

file = "out100.csv"
dataFormat = '%Y-%m-%d %H:%M:%S'
one_day = timedelta(1)

def parseUserInput(StartDateTime, EndDateTime):

    StartDateTime = StartDateTime.replace('T', ' ')[:StartDateTime.rfind('.')]
    StartDateTime = datetime.strptime(StartDateTime, dataFormat)
    StartDateTime = int(time.mktime(StartDateTime.timetuple())) // 100

    EndDateTime = EndDateTime.replace('T', ' ')[:EndDateTime.rfind('.')]
    EndDateTime = datetime.strptime(EndDateTime, dataFormat)
    EndDateTime = int(time.mktime(EndDateTime.timetuple())) // 100

    return [StartDateTime, EndDateTime]

def read(file):
    f = open(file)
    
    data = f.read()

    f.close()

    return data

def parseString(str):
    return str.split(",")

def printArray(arr):
    for i in arr:
        print(i)

def parseInput(data, dataFormat):

    dict = {}

    data = data.split('\n')
        
    for i in range(1, len(data) - 1):

        data[i] = data[i].split(',')
        data[i][1] = data[i][1].replace('T', ' ')[:data[i][1].rfind('.')]
        data[i][1] = datetime.strptime(data[i][1], dataFormat)

        data[i][2] = data[i][2].replace('T', ' ')[:data[i][2].rfind('.')]
        data[i][2] = datetime.strptime(data[i][2], dataFormat)

        unix1 = int(time.mktime(data[i][1].timetuple())) // 100
        unix2 = int(time.mktime(data[i][2].timetuple())) // 100


        st = str(unix1) + "," + str(unix2)

        if data[i][0] in dict:
            dict.update({data[i][0]:dict[data[i][0]] + "," + st})
        else:
            dict.update({data[i][0]:st})
    return dict

def parseInputUnOptimized(data):
    data = data.split('\n')

    for i in range(1, len(data)-1):

        data[i] = data[i].split(',')
        data[i][1] = data[i][1].replace('T', ' ')[:data[i][1].rfind('.')]
        data[i][1] = datetime.strptime(data[i][1], dataFormat)

        data[i][2] = data[i][2].replace('T', ' ')[:data[i][2].rfind('.')]
        data[i][2] = datetime.strptime(data[i][2], dataFormat)
    
    data = data[1:]

    return data

dict = parseInput(read(file), dataFormat)

@app.route('/')
def main():
    return render_template("main.html")

@app.route("/isTheIDFree", methods=["POST", "GET"])
def showIsTheIDFree():
    if request.method == "POST":
        id = request.form.get("id")
        start = request.form.get("start")
        end = request.form.get("end")
        return redirect(url_for("isTheIDFree", ResourseID=id, StartDateTime=start, EndDateTime = end))
    else:
        return render_template("isTheIDFree.html")

@app.route("/searchId", methods=["POST", "GET"])
def showSearchId():
    if request.method == "POST":
        start = request.form.get("start")
        end = request.form.get("end")
        return redirect(url_for("searchId",StartDateTime=start, EndDateTime = end))
    else:
        return render_template("searchId.html")

@app.route('/isTheIDFree/<ResourseID>&<StartDateTime>&<EndDateTime>')
def isTheIDFree(ResourseID, StartDateTime, EndDateTime):

    tmp = parseUserInput(StartDateTime, EndDateTime)

    StartDateTime = tmp[0]
    EndDateTime = tmp[1]

    if str(ResourseID) is dict: 
        arr = dict[ResourseID].split(",")
    else: 
        return render_template("notFreeID.html")
    
    ans = "Id is free"
    
    for i in range(0, len(arr), 2):
        if (check([int(arr[i]), int(arr[i + 1])], StartDateTime, EndDateTime)):
            ans = 'Id is not free'
            break
    
    if ans: return render_template("freeID.html")
    else: return render_template("notFreeID.html")

@app.route('/searchId/<StartDateTime>&<EndDateTime>')
def searchId(StartDateTime, EndDateTime):
    ids = set()
    closed = set()

    tmp = parseUserInput(StartDateTime, EndDateTime)

    for i in dict:
        arr = dict[str(i)].split(",")
        for val in range(0, len(arr), 2):
            if (i in closed) or check([int(arr[val]), int(arr[val + 1])], tmp[0], tmp[1]):
                closed.add(i)
                if val in ids:
                    ids.remove(i)
            else:
                ids.add(i)
    
    return str(ids)


def check(hotel, dateStart, dateEnd):
  return (
    (hotel[0] <= dateStart < hotel[1]) 
    or  
    (hotel[0] < dateEnd <= hotel[1]) 
    or
    (dateStart <= hotel[0] < dateEnd) 
    or
    (dateStart < hotel[1] <= dateEnd)
  )

def retId(data):
    res = []
    for i in range(0, len(data)-1):
        if data[i][0] not in res:
            res.append(data[i][0])
    res.sort()
    return res

def resultBool(date, data, id):
    a = 'Free'

    dateStart = date
    dateEnd = date + one_day
    for i in range(0, len(data)-1):
        if id == data[i][0]:
            if (
                dateStart < data[i][1] < dateEnd
                or data[i][1] < dateStart < data[i][2]
                or dateStart == data[i][1] and dateEnd == data[i][2]
                or dateStart == data[i][1] and dateEnd != data[i][2]
                or dateStart != data[i][1] and dateEnd == data[i][2]
            ):
                a = 'Busy'
                break
    return a

def createTable(data):
    table = []

    table.append([])
    table[0].append('ResourseId')

    start = '2021-01-01'
    start = datetime.strptime(start, '%Y-%m-%d')

    #print(type(data[0][1]))

    for i in range(1, 366):
        table[0].append(start)
        start += one_day

    for i in range(1, len(res)+1):
        table.append([])
        table[i].append(res[i-1])
        for j in range(1,366):
            table[i].append(resultBool(table[0][j], data, table[i][0]))

    return table

data = read('out10.csv')
data = parseInputUnOptimized(data)
res = retId(data)
table = createTable(data)

@app.route('/table')
def fillingTable():
    return render_template('table.html', data=table)

if __name__ == "__main__":
    app.run(debug = True)

