from datetime import datetime 
import timeit

def isTheIDFree(array, ResourseID, StartDateTime, EndDateTime):
    a = True
    for i in range(1, len(array)):
        if array[i][0] == ResourseID:
            """Проверка на пересечение"""
            if (
                StartDateTime < array[i][1] < EndDateTime 
                or array[i][1] < StartDateTime < array[i][2]
                or StartDateTime == array[i][1] and EndDateTime == array[i][2]
                or StartDateTime == array[i][1] and EndDateTime != array[i][2]
                or StartDateTime != array[i][1] and EndDateTime == array[i][2]
            ):
                a = False
                break

    return a

def searchID(array, StartDateTime, EndDateTime):
    ID = {}
    for i in range(1, len(array)):
        if not (
                StartDateTime < array[i][1] < EndDateTime 
                or array[i][1] < StartDateTime < array[i][2]
                or StartDateTime == array[i][1] and EndDateTime == array[i][2]
                or StartDateTime == array[i][1] and EndDateTime != array[i][2]
                or StartDateTime != array[i][1] and EndDateTime == array[i][2]
            ):
            if array[i][0] not in ID:
                ID.update({array[i][0]: 1})
        else:
            ID.update({array[i][0]: 0}) 

    resID = []
    for key in ID:
        if ID[key] == 1:
            resID.append(key)

    return resID


def giveArrayFromFile(file):
    dataFormat = '%Y-%m-%d %H:%M:%S'

    with open(file) as file:
        array = [row.strip() for row in file]

    for i in range(0, len(array)):
        array[i] = array[i].split(',')

    for i in range(1, len(array)):
        array[i][1] = str(array[i][1])
        array[i][1] = datetime.strptime(str(array[i][1][0: 10]) + ' ' + str(array[i][1][11: len(dataFormat) + 1]), dataFormat)
        array[i][2] = str(array[i][2])
        array[i][2] = datetime.strptime(array[i][2][0: 10] + ' ' + array[i][2][11: len(dataFormat) + 1], dataFormat)
        # print (array[i])
    return array

def main():
    file = "out1000.csv"

    dataFormat = '%Y-%m-%d %H:%M:%S'

    # ResourseID = input("ID: ")
    # print(f'Введите начало и конец даты в формате "{dataFormat}"')
    # StartDateTime = str(input("Начало: "))
    # EndDateTime = str(input("Конец: "))

    ResourseID = 26
    StartDateTime = "2020-07-13 20:00:00"
    EndDateTime = "2020-07-13 20:30:00"


    StartDateTime = datetime.strptime(StartDateTime, dataFormat)
    EndDateTime = datetime.strptime(EndDateTime, dataFormat)

    array = giveArrayFromFile(file)

    print("Занятость ID в заданное время: ")
    if isTheIDFree(array, ResourseID, StartDateTime, EndDateTime):
        print("ID свободен")
    else:
        print("ID занят")


    print("Поиск свободных ID по заданному времени: ")
    print(searchID(array, StartDateTime, EndDateTime))
    return [array, ResourseID, StartDateTime, EndDateTime]




if __name__ == "__main__":
    val = main()
    print(timeit.timeit("searchID(val[0], val[2], val[3])", setup="from __main__ import searchID, val", number=1))

